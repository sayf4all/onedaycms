<#import "/spring.ftl" as spring>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"[]>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US" xml:lang="en">
<head>
    <!--
    Base template (without user's data) checked by http://validator.w3.org : "This page is valid XHTML 1.0 Transitional"
    -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Description"/>
    <meta name="keywords" content="Keywords"/>


    <link rel="stylesheet" href="/static/css/style.css" type="text/css" media="screen"/>
    <!--[if IE 6]>
    <link rel="stylesheet" href="/static/css/style.ie6.css" type="text/css" media="screen"/><![endif]-->
    <!--[if IE 7]>
    <link rel="stylesheet" href="/static/css/style.ie7.css" type="text/css" media="screen"/><![endif]-->
    <link href="/static/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/>

    <!-- we code these -->
    <link href="/static/css/dropzone.css" type="text/css" rel="stylesheet"/>

    <script type="text/javascript" src="/static/js/libs/jquery.1.9.1.min.js"></script>
    <script src="/static/js/libs/vendor/jquery.ui.widget.js"></script>
    <script src="/static/js/libs/jquery.iframe-transport.js"></script>
    <script src="/static/js/libs/jquery.fileupload.js"></script>

    <script src="/static/js/all.js"></script>
    <script src="/static/js/myuploadfunction.js"></script>

<#--<script type="text/javascript" src="/static/js/script.js"></script>-->
    <script type="text/javascript" src="/static/js/all.js"></script>

</head>
<body>




<h1 class="art-logo-name"><a href="#"><@spring.message 'common.siteheader'/></a></h1>

<h2 class="art-logo-text"><@spring.message 'common.sitesubheader'/></h2>


