<#assign page = 'home'>
<#include 'header.ftl'>
<#include '../common/messages.ftl'>



<div class="art-content-layout">
<div class="art-content-layout-row">

<#include '../common/sidebar.ftl'>

<div class="art-layout-cell art-content">
    <div class="art-post">
        <div class="art-post-body">
            <div class="art-post-inner art-article">
                Contatgegevens:
                <ul>
                    <li>
                        <div><@spring.message 'contact.location'/></div>
                        <div><@spring.message 'contact.address'/></div>
                        <div><@spring.message 'contact.telephone'/></div>
                        <div><@spring.message 'contact.email'/></div>
                    </li>
                </ul>
            </div>

            <div class="cleared"></div>
        </div>
    </div>
    <div class="cleared"></div>
</div>



<#include '../common/footer.ftl'>