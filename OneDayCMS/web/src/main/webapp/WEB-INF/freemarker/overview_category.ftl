<table>
    <caption><@spring.message 'category.title.plural'/></caption>
    <thead>
    <tr>
        <td>id</td>
        <td>naam</td>
        <td>actie</td>
    </tr>
    </thead>
    <tbody>
    <#list categories as category>
    <tr>
        <td>${category.id?html}</td>
        <td>${category.name?html}</td>
        <td>actie</td>
    </tr>
    </#list>
    </tbody>
</table>
