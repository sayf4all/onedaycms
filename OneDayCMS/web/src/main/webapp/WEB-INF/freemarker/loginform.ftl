    <form action="j_spring_security_check" method="post" >

        <label for="j_username"><@spring.message 'login.user'/></label>
        <input id="j_username" name="j_username" size="20" maxlength="50" type="text"/>
        <label for="j_password"><@spring.message 'login.pass'/></label>
        <input id="j_password" name="j_password" size="20" maxlength="50" type="password"/>
        <input type="submit" value="<@spring.message 'common.login'/>"/>
    </form>
