
        <form action="/item/add" method="post" class="appnitro" id="new_item_form">
        <div class="form_description">

            <ul>

                <li>


                    <div>
                        <label for="itemName" class="description">Item </label>
                        <input type="text" maxlength="255" class="element text medium" name="itemName" id="itemName"
                                >
                    </div>
                    <div>
                        <label for="priceInput" class="description">Price &euro;</label>
                            <span>
                                <input type="text" size="10" class="element text currency" name="priceInput"
                                       id="priceInput"  >
                            </span>
                    </div>

                    <div>
                        <label for="shortDescription" class="description">Decription </label>
                        <textarea class="element textarea medium" name="shortDescription"
                                  id="shortDescription"> </textarea>
                    </div>
                </li>

                <li class="buttons">
                    <input type="button" value="Save" class="art-button" id="saveItem">
                </li>
            </ul>
        </form>