<ul class="art-hmenu">

    <li>
        <a href="/" class="active"><span class="l"></span><span class="r"></span><span class="t">Home</span></a>
    </li>

    <li>
        <a href="/item/overview"><span class="l"></span><span class="r"></span><span class="t">Items</span></a>
        <ul>
            <li>
                <a href="/item/overview">overzicht</a>
            </li>

            <li>
                <a href="/item/new">toevoegen</a>
            </li>


        </ul>
    </li>

    <li>
        <a href="/user/overview"><span class="l"></span><span class="r"></span><span class="t">Users</span></a>
        <ul>
            <li>
                <a href="/user/overview">overzicht</a>
            </li>
            <li>
                <a href="/user/new">toevoegen</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="/category/overview"><span class="l"></span><span class="r"></span><span class="t">Categorieën</span></a>
        <ul>
            <li>
                <a href="/category/overview">overzicht</a>
            </li>
            <li>
                <a href="/category/new">toevoegen</a>
            </li>
        </ul>
    </li>


    <!--<li>
        <a href="/uploadfile"><span class="l"></span><span class="r"></span><span class="t">Upload</span></a>
    </li>-->


    <li>
        <a href="/user/profile"><span class="l"></span><span class="r"></span><span class="t">My Profile</span></a>
    </li>

    <li>
        <a href="/contact"><span class="l"></span><span class="r"></span><span class="t">Contact</span></a>
    </li>
    <div align="right" id="logout" onclick="logout();false;"><#if loggedIn??>
        welkom <strong>${userName}</strong> --- <@spring.message 'common.logout'/>.

    </div>
</ul>


