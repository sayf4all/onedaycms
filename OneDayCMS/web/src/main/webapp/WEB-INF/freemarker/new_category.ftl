<form action="/category/add" method="post" class="appnitro" id="new_category_form">
    <div class="form_description">

        <h2><@spring.message 'category.new' /></h2>

        <p>fill in the item details and save</p>
    </div>
    <ul>

        <li>
            <label for="categoryName" class="description">Category</label>

            <div>
                <input type="text" maxlength="255" class="element text medium" name="categoryName" id="categoryName">
            </div>

            <div>
                <label for="parentId" class="description">Parent </label>
                <textarea class="element textarea medium" name="parentId" id="parentId"></textarea>
            </div>
        </li>

        <li class="buttons">
            <input type="submit" value="Save" id="saveForm"">
        </li>
    </ul>
</form>