<h1>Version</h1>
<p>

    <strong>Version nr:</strong> ${version?html}<br/>

    <strong>Create date nr:</strong> ${createDate?html}<br/>
    <strong>Hostname:</strong> ${hostName?html}<br/>
    <strong>Created by:</strong> ${createdBy?html}<br/>
    <strong>JDK version :</strong> ${compiledByJdkVersion?html}<br/>
    <strong>Session and Instance :</strong> ${sessionID?html}<br/>
</p>


