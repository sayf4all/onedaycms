todo: adjust for TXOSubscription

Template Variables
==================

This document describes the variables that will be exposed to the templates by the model.
By convention we use the following two template names :

* form              : Add or edit items
* overview          : Show the items


Cms
------

These variables and functions will be available in all templates

* cmsItem						Map<String,String> The current Content Item of the cms
* cmsItem.page_title		String					The current page title
* cmsItem.meta_title		String
* cmsItem.intro				Markup

* cms.text 'path'				String					A text located in the cms. Example:		<@cms.text 'wachtwoord_vergeten_content' />
* cms.getitem 'path'		Map<String,Stirng>	a Content Item located at path.
* cms.getItems 'path'		List<Map<String,String>>	a list of contentitems located directly beneath path. Example:		<#list cms.getItems("/") as item>


Only for the homepage:

* cmsItem.meta_description


Default
-------

These variables will be available in all templates.

* fullName          : String (The full name of the current user)
* errors            : List<String> (Error message keys)
* messages          : List<String> (Notifcation message keys)


Organization
------------
ABZ

### form

* organization      : Organization (When editing an organization)

### Paginated List (overview)

* organizations.pageList		List<Organization>
* organizations.firstLinkedPage	first page number in pagination
* organizations.lastLinkedPage	last page number in pagination
* organizations.page					current page number
* organizations.sort.property		propertyname the list is currently sorted by
* organizations.sort.ascending		indicates whether the list is currently sorted ascending

* urls.new          : Create a new organization
* urls.edit         : Edit an organization
* urls.delete       : Delete an organization
* urls.overview     : List all organizations

### form

* user              : User (When editing a user)
* organizations     : List<Organization>
* urls.password     : Reset the user's password (When editing a user)

### overview

* users             : List<User>
* organization      : List<Organization>

* urls.new          : Create a new user
* urls.edit         : Edit a user
* urls.delete       : Delete a user
* urls.overview     : List all users

AuditLog
--------

### overview

* auditLogEntries   : List<AuditLogEntry>
* action            : String
* status            : String
* dateStart         : String dd/mm/yyyy
* dateEnd           : String dd/mm/yyyy

Account
-------

### form

* user              : User

### overview

* user              : User
