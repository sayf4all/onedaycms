<#assign page = 'edit'>
<#include 'header.ftl'>
<#include '../common/messages.ftl'>
<div class="art-content-layout">
    <div class="art-content-layout-row">

    <#include 'common/sidebar.ftl'>

        <div class="art-layout-cell art-content">




            <div class="art-post">
                <div class="art-post-body">
                    <div class="art-post-inner art-article">

                        <div id="form_container">

                        <div class="form_description">

                        <#if action?? && action="viewProfile">
                            <h2><@spring.message 'user.view.profile' /></h2>
                            <p>fill in the user details and save</p>
                        </div>
                            <div>
                                <table width="100%" border="0px" style="overflow: hidden;">
                                    <tr>
                                        <td>
                                            <table width="100%">

                                                    <tr>
                                                        <td width="100px">Gebruikersnaam</td>
                                                        <td>${username}<br>
                                                    </tr>
                                                    <tr>
                                                        <td>Voornaam</td>
                                                        <td>${firstname}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Achternaam</td>
                                                        <td>${lastname}</td>
                                                    </tr>

                                            </table>
                                        </td>
                                        <td width="150px">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <img src="${profileImage}" width="150px" alt="profile picture">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>



                                <input type="hidden" name="id" value="${id}">
                            </div>
                            <div class="art-layout-cell art-content">

                                <div style="width:500px;padding:20px">
                                    <div><h2>Upload files here</h2></div>
                                    <input id="fileupload" type="file" name="files[]" data-url="/upload" multiple>

                                    <div id="progress" class="progress">
                                        <div class="bar" style="width: 0%;"></div>
                                    </div>

                                    <table id="uploaded-files" class="table">
                                        <tr>
                                            <th td width="200px" style="overflow: hidden;">File Name</th>
                                            <th>File Type</th>
                                            <th>Download</th>
                                            <th>Profile</th>
                                            <th>Actie</th>
                                        </tr>
                                        <#if fileList??>
                                            <#list fileList as file>
                                                <tr>
                                                    <td>${file.filename?html}</td>
                                                    <td>${file.type?html}</td>
                                                    <td><a href="/data/get/${file.id}"><img src="/data/get/mini/${file.id}" <#if file.notes??>alt="${file.notes}"</#if> /></a></td>
                                                    <td><input type="button" value="set profile image" onclick="submitProfileImage(${file.id})"/></td>
                                                    <td>
                                                        <a href="#" onclick="">
                                                            <img src="/static/assets/layout/images/spacer.gif" width="1" height="1"
                                                                 style="width:22px; height:22px; background:url(/static/assets/layout/data-uri/button-actions.png) -21px 0;") "/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </#list>

                                        </#if>
                                    </table>

                                </div>


                                <div class="cleared"></div>
                            </div>
                        </#if>
                        </div>
                    </div>
                </div>
            </div>











            <div class="cleared"></div>
        </div>
<#include 'common/footer.ftl'>