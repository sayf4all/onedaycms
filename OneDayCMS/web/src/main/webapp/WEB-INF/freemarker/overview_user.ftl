<table>
    <caption>Users overview</caption>

    <tbody>
    <#list userList as user>
    <tr>
        <td>${user.id}</td>
        <td>${user.username}</td>
        <td>${user.firstName}</td>
        <td>${user.lastName}</td>
        <td>********</td>
        <td>

            <a href="#" onclick="submitDeleteUser('${user.username}')">
                <img src="/static/assets/layout/images/spacer.gif" width="1" height="1"
                     style="width:22px; height:22px; background:url(/static/assets/layout/data-uri/button-actions.png) -21px 0;")
                "/>
            </a>
            ||
            <a class="art-button" href="/user/edit/${user.username}">
                <img src="/static/assets/layout/images/spacer.gif" width="1" height="1"
                     style="width:22px; height:22px; background:url(/static/assets/layout/data-uri/button-actions.png) 0 0;")
                "/>
            </a>
        </td>
    </tr>
    </#list>
    </tbody>
</table>
<p>${userList?size} users found</p>
