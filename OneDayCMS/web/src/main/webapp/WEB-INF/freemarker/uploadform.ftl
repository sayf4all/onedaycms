<#assign page = 'edit'>
<#include 'header.ftl'>
<#include '../common/messages.ftl'>
<div class="art-content-layout">
    <div class="art-content-layout-row">

    <#include 'common/sidebar.ftl'>

        <div class="art-layout-cell art-content">

            <div style="width:500px;padding:20px">

                <input id="fileupload" type="file" name="files[]" data-url="/upload" multiple>

                <div id="dropzone" class="fade well">Drop files here</div>

                <div id="progress" class="progress">
                    <div class="bar" style="width: 0%;"></div>
                </div>

                <table id="uploaded-files" class="table">
                    <tr>
                        <th>File Name</th>
                        <th>File Size</th>
                        <th>File Type</th>
                        <th>Download</th>
                    </tr>
                </table>

            </div>


            <div class="cleared"></div>
        </div>
<#include 'common/footer.ftl'>