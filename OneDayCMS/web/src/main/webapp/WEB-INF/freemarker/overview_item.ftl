
<#include 'header.ftl'>

<table>
    <caption>Items overview</caption>

    <tbody>
    <#list itemList as item>
    <tr>
        <td>${item.id?html}</td>
        <td>${item.name?html}</td>
        <td>${item.price?html}</td>
        <td>${item.shortDescription?html}</td>

        <td>
            <a href="#" onclick="submitDeleteItem(${item.id})">
                <img src="/static/assets/layout/images/spacer.gif" width="1" height="1"
                     style="width:22px; height:22px; background:url(/static/assets/layout/data-uri/button-actions.png) -21px 0;")"/>
            </a>
            ||
            <a class="art-button" href="/item/edit/${item.id}">edit</a>
        </td>
        <td>
            <input name="aantal_${item.id}" id="aantal_${item.id}" type="text" value="0" style="width: 50px;">
        </td>
        <td>

            <input id="orderItemButton" type="button" value="order" onclick="submitorderItem(${item.id})"></span>
        </td>
    </tr>
    </#list>
    </tbody>
</table>