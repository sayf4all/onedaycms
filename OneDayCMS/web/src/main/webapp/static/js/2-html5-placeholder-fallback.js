$(function() {
	var placeholderElement = document.createElement('input');
	if (!('placeholder' in placeholderElement)) {
		$('input[placeholder],textarea[placeholder]').each(function() {
			var placeholderText = $(this).attr('placeholder');
			$(this)
				.addClass('placeholder')
				.val(placeholderText)
				.focus(function() {
					if ($(this).val() === placeholderText) {
						$(this).removeClass('placeholder').val('');
					}
				})
				.blur(function() {
					if ($(this).val() === '') {
						$(this).addClass('placeholder').val(placeholderText);
					}
				});
		});
	}
});