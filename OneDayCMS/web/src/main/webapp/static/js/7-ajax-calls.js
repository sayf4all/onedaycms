function addItem() {

    var name = $('#itemName').val();
    var priceInput = $('#priceInput').val();
    var description = $('#description').val();
    var url = "/item/add/" + name + "/" + priceInput + "/" + description;

    $.ajax({
        type:"GET",
        url:url
    }).done(function (data) {
            var resultObject = JSON.parse(data);
            if (resultObject['success'] == true) {
                var feedBackMessage = 'Item is aangemaakt.<br>';
                var href = "/item/new";
                var question = false;
                showPopUp(feedBackMessage, href, question);

            } else if (resultObject['success'] == false) {
                var feedBackMessage = 'Item aanmaken niet gelukt';
                var href = $(this).attr('href');
                var question = false;

                showPopUp(feedBackMessage, href, question);
                e.preventDefault();

            }
        });
}

$("#saveForm").click(function() {
    document.forms["new_category_form"].submit();
});

function submitDeleteItem(id) {

    var url = "/item/delete/" + id;
    $.ajax({
        type:"GET",
        url:url
    }).done(function (data) {
            var resultObject = JSON.parse(data);
            if (resultObject['success'] == true) {
                var feedBackMessage = 'Item is verwijderd.<br> Wilt u terug naar het overzicht?';
                var href = "/item/overview";
                var question = true;
                showPopUp(feedBackMessage, href, question);
            } else if (resultObject['success'] == false) {
                var feedBackMessage = 'Item verwijderen is niet gelukt';
                var href = $(this).attr('href');
                var question = false;

                showPopUp(feedBackMessage, href, question);
                e.preventDefault();
            }
        });

}

function submitDeleteUser(name) {

    var url = "/user/delete/" + name;
    $.ajax({
        type:"GET",
        url:url
    }).done(function (data) {
            var resultObject = JSON.parse(data);
            if (resultObject['success'] == true) {
                var feedBackMessage = 'User is verwijderd.<br> Wilt u terug naar het overzicht?';
                var href = "/user/overview";
                var question = true;
                showPopUp(feedBackMessage, href, question);
            } else if (resultObject['success'] == false) {
                var feedBackMessage = 'user verwijderen is niet gelukt';
                var href = $(this).attr('href');
                var question = false;

                showPopUp(feedBackMessage, href, question);
                e.preventDefault();
            }
        });

}

function submitProfileImage(id) {

    var url = "/user/setprofileimage/" + id;
    $.ajax({
        type:"GET",
        url:url
    }).done(function (data) {
            var resultObject = JSON.parse(data);
            if (resultObject['success'] == true) {
                window.location = '/user/profile';
            } else if (resultObject['success'] == false) {
                var feedBackMessage = 'Item verwijderen is niet gelukt';
                var href = $(this).attr('href');
                var question = false;

                showPopUp(feedBackMessage, href, question);
                e.preventDefault();
            }
        });

}

$(function submitNewUser() {

    //---------------------------
    // variable to hold request
    var request;
    // bind to the submit event of our form
    $("#new_user_form").submit(function (event) {
        // abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);
        // let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");
        // serialize the data in the form
        var serializedData = $form.serialize();

        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);

        // fire off the request to /form.php
        request = $.ajax({
            url:"/user/add",
            type:"post",
            data:serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            // log a message to the console
            var resultObject = JSON.parse(response);
            var feedBackMessage = '';
            if(resultObject['success']===false){
                feedBackMessage = 'User aanmaken is niet gelukt';
            } else if(resultObject['success']===true){
                feedBackMessage = 'User aanmaken is gelukt';
            }
            var href = $(this).attr('href');
            var question = false;

            showPopUp(feedBackMessage, href, question);
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.error(
                "The following error occured: " +
                    textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $inputs.prop("disabled", false);
        });

        // prevent default posting of form
        event.preventDefault();
    });
    //---------------------------


});

$(function submitUpdateUser() {

    //---------------------------
    // variable to hold request
    var request;
    // bind to the submit event of our form
    $("#update_user_form").submit(function (event) {
        // abort any pending request
        if (request) {
            request.abort();
        }
        // setup some local variables
        var $form = $(this);
        // let's select and cache all the fields
        var $inputs = $form.find("input, select, button, textarea");
        // serialize the data in the form
        var serializedData = $form.serialize();

        // let's disable the inputs for the duration of the ajax request
        $inputs.prop("disabled", true);

        // fire off the request to /form.php
        request = $.ajax({
            url:"/user/add",
            type:"post",
            data:serializedData
        });

        // callback handler that will be called on success
        request.done(function (response, textStatus, jqXHR) {
            // log a message to the console
            var resultObject = JSON.parse(response);
            var feedBackMessage = '';
            if(resultObject['success']===false){
                feedBackMessage = 'User aanmaken is niet gelukt';
            } else if(resultObject['success']===true){
                feedBackMessage = 'User aanmaken is gelukt';
            }
            var href = $(this).attr('href');
            var question = false;

            showPopUp(feedBackMessage, href, question);
        });

        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // log the error to the console
            console.error(
                "The following error occured: " +
                    textStatus, errorThrown
            );
        });

        // callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            // reenable the inputs
            $inputs.prop("disabled", false);
        });

        // prevent default posting of form
        event.preventDefault();
    });
    //---------------------------


});