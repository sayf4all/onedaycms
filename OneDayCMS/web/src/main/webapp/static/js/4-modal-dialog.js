/**
 * Modal Dialog component
 */
OneDay.ModalDialog = new function() {

	var api = {},
		eventsBound = false,
		active = false,
		config = {
			'message': 'Default message',
			'question': false,             // true for Yes/No buttons, false for Ok button
			'overlayClose': false,         // true for close click on overlay
			'onOpen': function() {},       // fired on dialog open
			'onClose': function(answer) {} // fired on dialog close, answer (string) is passed (close/yes/no/ok)
		};

	// Create a new Modal Dialog box, passing config parameters
	api.create = function(configParams) {

		// Only one dialog can be open at the same time
		active = true;

		// Read config settings
		readConfig(configParams);

		// Prepare dialog in DOM
		prepareDialog();

		// Fire show event
		config.onOpen();

		// Show dialog
		$('.modal-dialog, .modal-dialog-overlay').show();

		return true;
	};

	// Update config with passed configParams
	var readConfig = function(configParams) {
		$.each(configParams, function(key, value) {
			if (config[key] !== undefined) {
				config[key] = value;
			}
		});
	};

	// Prepare dialog in DOM
	var prepareDialog = function() {

		// Insert message into DOM
		$('.modal-dialog p').html(config.message);

		// Question? Show 'yes' and 'no' buttons
		if (config.question) {
			$('.modal-dialog button[data-answer!=ok]').show();
		}

		// Message? Show 'ok' button
		if (!config.question) {
			$('.modal-dialog button[data-answer=ok]').show();
		}

		// Bind events
		bindEvents();
	};

	// Bind events
	var bindEvents = function() {

		// Events may only be bound once
		if (eventsBound) {
			return;
		}

		// Bind events
		$('.modal-dialog button, .modal-dialog-overlay').on('click', closeDialogEvent);

		eventsBound = true;
	};

	// Close event function
	var closeDialogEvent = function(e) {

		var answer = $(this).attr('data-answer');

		// Click on background, while buttonClose is disabled
		if (!config.overlayClose && answer === 'close') {
			return;
		}

		// Fire close event
		config.onClose(answer);

		// Hide dialog
		$('.modal-dialog, .modal-dialog button, .modal-dialog-overlay').hide();

		// Set 'active' to false, so another dialog may be opened
		active = false;
	};

	return api;
};