$(function () {
    $('#fileupload').fileupload({
        dataType:'json',

        done:function (e, data) {
            $.each(data.result, function (index, file) {

                file = JSON.parse(file);
                if (file.filename != null) {
                    window.location = '/user/profile';
                }

            });
        },

        progressall:function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
            if (progress == 100) {
                window.location = '/user/profile';
            }
        },

        dropZone:$('#dropzone')
    });
});