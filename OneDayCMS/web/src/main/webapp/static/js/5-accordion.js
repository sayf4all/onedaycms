/**
 * Accordion component
 */
OneDay.Accordion = new function() {

	var animLength = 1000,
		references = {};

	/**
	 * Start init on all accordion components
	 */
	this.init = function() {
		$('[data-component=Accordion]').each(function() {
			references[this.id] = new Accordion(this);
		});
	};

	/**
	 * Init a single accordion
	 */
	var Accordion = function(element) {

		$('h3', element).click(function(e) {

			e.preventDefault();
			$(this).blur();

			var $clickedH3  = $(this),
				$clickedDiv = $clickedH3.next(),
				$otherH3s   = $('h3', element).not($clickedH3),
				$otherDivs  = $otherH3s.next();

			// Close all except current
			$otherDivs.slideUp(animLength);
			$otherH3s.removeClass('active');

			// Toggle current
			if ($clickedDiv.is(':visible')) {
				$clickedDiv.slideUp(animLength);
				$clickedH3.removeClass('active');
			} else {
				$clickedDiv.slideDown(animLength);
				$clickedH3.addClass('active');
			}
		});
	};

};