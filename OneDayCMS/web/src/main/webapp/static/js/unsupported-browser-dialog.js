/**
 * Unsupported Browser Dialog
 */
var UnsupportedBrowserDialog = new function() {

	var urls = {
		'ie':           'http://windows.microsoft.com/nl-NL/internet-explorer/products/ie/home',
		'chromeframe':  'http://code.google.com/intl/nl/chrome/chromeframe/', // ?destination= | ?redirect=true
		'chrome':       'http://www.google.nl/chrome?hl=nl',
		'firefox':      'http://www.mozilla.org/en-US/firefox/new/',
		'safari':       'http://www.apple.com/safari/',
		'opera':        'http://www.opera.com/'
	};

	// Only show popup when Chrome Frame is not yet installed, and the cookie is not yet set
	var init = function() {
		if (hasChromeFrame() || hasCookie()) {
			return;
		}
		showDialog();
	};

	// Check for Chrome Frame
	var hasChromeFrame = function() {
		return navigator.userAgent.indexOf('chromeframe') !== -1;
	};

	// Check for cookie
	var hasCookie = function() {
		return document.cookie.indexOf('ignore-unsupported-browser') !== -1;
	};

	// Insert dialog into DOM
	var showDialog = function() {

		var template =
			'<div class="unsupported-browser-dialog-overlay"></div>' +
			'<div class="unsupported-browser-dialog">' +
				'<h1>U gebruikt een verouderde versie van Internet Explorer</h1>' +
				'<h2>Download de laatste versie van <a href="[url.ie]">Internet Explorer</a>.</h2>' +
				'<h2>Of download een browser uit onderstaand overzicht.</h2>' +
				'<p>Deze browsers zijn veelal sneller en veiliger en voldoen beter aan de webstandaarden.</p>' +
				'<div class="browser">' +
					'<a class="chrome" href="[url.chrome]"><span>Download Chrome</span></a>' +
					'<a class="firefox" href="[url.firefox]"><span>Download Firefox</span></a>' +
					'<a class="safari" href="[url.safari]"><span>Download Safari</span></a>' +
					'<a class="opera" href="[url.opera]"><span>Download Opera</span></a>' +
				'</div>' +
				'<h2>Ik kan mijn browser niet updaten</h2>' +
				'<p>Gebruikt u een oude versie van Internet Explorer en kunt u deze niet opdaten? <a href="[url.chromeframe]">Activeer Google Chrome Frame</a> om beter gebruik te maken van de functionaliteiten op deze website.</p>' +
				'<a class="close" href="#"></a>' +
				'<div class="rounded-corner top-left"></div><div class="rounded-corner top-middle"></div><div class="rounded-corner top-right"></div>' +
				'<div class="rounded-corner bottom-left"></div><div class="rounded-corner bottom-middle"></div><div class="rounded-corner bottom-right"></div>' +
			'</div>';

		var html = template
			.replace('[url.ie]',          urls.ie)
			.replace('[url.chromeframe]', urls.chromeframe)
			.replace('[url.chrome]',      urls.chrome)
			.replace('[url.firefox]',     urls.firefox)
			.replace('[url.safari]',      urls.safari)
			.replace('[url.opera]',       urls.opera);

		$('body').append(html);

		// Bind click on close
		$('.unsupported-browser-dialog .close').click(function(e) {

			e.preventDefault();

			// Set cookie
			var expires = new Date();
			expires.setTime((new Date()).getTime() + (364 * 24 * 60 * 60 * 1000));
			document.cookie = 'ignore-unsupported-browser=1; expires=' + expires.toGMTString() + '; path=/';

			// Close dialog
			$('.unsupported-browser-dialog, .unsupported-browser-dialog-overlay').remove();
		});
	};

	init();

};