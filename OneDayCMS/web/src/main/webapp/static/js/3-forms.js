/**
 * Form components
 */
OneDay.forms = new function() {

	var component  = {},
		references = {};

	this.initialize = function() {
		$('[data-component]:not([data-initialized])').each(function() {
			if (component[this.getAttribute('data-component')] !== undefined) {
				this.setAttribute('data-initialized', true);
				references[this.id] = new component[this.getAttribute('data-component')](this);
			}
		});
	};

	/**
	 * Text field
	 */
	component.TextField = function(element) {
		$('input[type=text],input[type=password]', element).each(function() {
			var $this   = $(this),
				$parent = $this.parent();

			$this
				.focus(function(e) {
					$parent.addClass('focus');
				})
				.blur(function(e) {
					$parent.removeClass('focus');
				});
		});
	};

	/**
	 * Textarea field
	 */
	component.TextareaField = function(element) {
		$('textarea', element).each(function() {
			var $this   = $(this),
				$parent = $this.parent();

			$this
				.focus(function(e) {
					$parent.addClass('focus');
				})
				.blur(function(e) {
					$parent.removeClass('focus');
				});
		});
	};

	/**
	 * Select field
	 */
	component.SelectField = function(element) {
		var $element      = $(element),
			$select       = $('select', element),
			$customSelect = $('<div class="scripted-select"/>').appendTo(element);

		$select
			.change(function(e) {
				$customSelect.text($('option:selected', $select).text());
			})
			.change()
			.focus(function(e) {
				$element.addClass('focus');
			})
			.blur(function(e) {
				$element.removeClass('focus');
			});
	};

};