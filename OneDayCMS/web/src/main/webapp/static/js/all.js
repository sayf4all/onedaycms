(function() {

	var include = function(filename) {
		document.write('<script src="' + filename + '"></script>');
	}

	include('/static/js/1-common.js');
	include('/static/js/2-html5-placeholder-fallback.js');
	include('/static/js/3-forms.js');
	include('/static/js/4-modal-dialog.js');
	include('/static/js/5-accordion.js');
	include('/static/js/6-onload.js');
    include('/static/js/7-ajax-calls.js');

}());

jQuery = null;