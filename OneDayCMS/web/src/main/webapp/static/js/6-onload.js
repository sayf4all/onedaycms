/**
 * On load
 */
$(function() {

	// Init forms
	OneDay.forms.initialize();

	// Init Accordion component
	OneDay.Accordion.init();

});

