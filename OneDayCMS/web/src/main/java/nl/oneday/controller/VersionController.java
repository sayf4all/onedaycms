package nl.oneday.controller;


/**
 * Created by IntelliJ IDEA. User: sjawad Date: 28-9-12 Time: 13:40 To change this template use File | Settings | File Templates.
 */

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;


@Controller
public class VersionController extends ControllerBase {

  private static final Logger LOG = LoggerFactory.getLogger(VersionController.class);

  public static final String TEMPLATE_REVISION = "version/version";
  public static final String VERSION = "version";
  public static final String CREATEDATE_ATTRIBUTE = "createDate";
  public static final String HOSTNAME_ATTRIBUTE = "hostName";
  public static final String BUILDNUMBER_ATTRIBUTE = "buildNumber";
  public static final String CREATEDBY_ATTRIBUTE = "createdBy";
  public static final String JDK_ATTRIBUTE = "compiledByJdkVersion";
  public static final String SESSIONID_ATTRIBUTE = "sessionID";
  public static final String ERROR_ATTRIBUTE = "errors";

  @Autowired
  private ResourceLoader resourceLoader;

  private String version = "Unknown";
  private String createDate = "Unknown";
  private String hostName = "Unknown";
  private String buildNumber = "Unknown";
  private String createdBy = "Unknown";
  private String compiledByJdkVersion = "Unknown";
  private String sessionID = "Unknown";


  @PostConstruct
  public void loadManifest() throws Exception {
    try {

      Resource manifestResource = resourceLoader.getResource("/META-INF/MANIFEST.MF");
      InputStream inputStream = manifestResource.getInputStream();
      Manifest manifest = new Manifest(inputStream);
      Attributes attributes = manifest.getMainAttributes();
      setRevisionPageAttributes(attributes);

    }
    catch (FileNotFoundException e) {
      LOG.warn("The manifest could not be openend. This is normal in the local development environment");
    }
    catch (Exception e) {
      LOG.warn("Error occurred during retrieval of version information", e);
    }
  }


  private void setRevisionPageAttributes(Attributes attributes) {
    try {
      version = attributes.getValue("Version") != null ? attributes.getValue("Version") : version;
      String dateTimeAsLong = attributes.getValue("Date") != null ? attributes.getValue("Date") : createDate;
      createDate = (dateTimeAsLong != null) ? dateTimeAsLong : "Not set";
      hostName = getLocalHostName();
      buildNumber = attributes.getValue("buildNumber") != null ? attributes.getValue("buildNumber") : buildNumber;
      createdBy = attributes.getValue("Built-By") != null ? attributes.getValue("Built-By") : createdBy;
      compiledByJdkVersion = attributes.getValue("Build-Jdk") != null ? attributes.getValue("Build-Jdk") : compiledByJdkVersion;
    }
    catch (Exception e) {
      LOG.warn("Error occurred during retrieval of version information", e);
    }
  }


  @RequestMapping(value = "/version", method = RequestMethod.GET)
  public String showRevision(Model model) {
    model.addAttribute(VERSION, getVersion());
    model.addAttribute(HOSTNAME_ATTRIBUTE, getLocalHostName());
    model.addAttribute(CREATEDATE_ATTRIBUTE, getCreateDate());
    model.addAttribute(BUILDNUMBER_ATTRIBUTE, getBuildNumber());
    model.addAttribute(CREATEDBY_ATTRIBUTE, getCreatedBy());
    model.addAttribute(JDK_ATTRIBUTE, getCompiledByJdkVersion());
    model.addAttribute(SESSIONID_ATTRIBUTE, getSessionID());

    return TEMPLATE_REVISION;
  }


  private String getLocalHostName() {
    try {
      java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
      return localMachine.getHostName();
    }
    catch (java.net.UnknownHostException uhe) {}
    return "Unknown";
  }


  public String getHostName() {
    return hostName;
  }


  public String getBuildNumber() {
    return buildNumber;
  }


  public String getCreatedBy() {
    return createdBy;
  }


  public String getCompiledByJdkVersion() {
    return compiledByJdkVersion;
  }


  public String getSessionID() {
    return RequestContextHolder.getRequestAttributes().getSessionId();
  }


  public String getVersion() {
    return version;
  }


  public String getCreateDate() {
    return createDate;
  }
}
