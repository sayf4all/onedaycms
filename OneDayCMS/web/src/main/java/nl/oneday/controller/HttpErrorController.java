package nl.oneday.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Handle HTTP errors.
 * 
 * @author Sayf Jawad <sayf4all@hotmail.com>
 */
@Controller
public class HttpErrorController extends ControllerBase {


  public static final String ERROR_PAGES_403 = "403";
  public static final String ERROR_PAGES_404 = "404";
  public static final String ERROR_PAGES_500 = "500";


  @RequestMapping(value = Paths.HTTP_403)
  public String render403(Model model) {
    setPageAttributes(model, "403");
    return ERROR_PAGES_403;
  }


  @RequestMapping(value = Paths.HTTP_404)
  public String render404(Model model) {
    setPageAttributes(model, "404");
    return ERROR_PAGES_404;
  }


  @RequestMapping(value = Paths.HTTP_500)
  public String render500(Model model) {
    setPageAttributes(model, "500");
    return ERROR_PAGES_500;
  }

}
