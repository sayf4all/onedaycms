package nl.oneday.controller;


import nl.oneday.data.domain.items.Item;
import nl.oneday.data.service.CategoryService;
import nl.oneday.data.service.ItemService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Web interface handling for Subscription.
 * 
 * @author Sayf Jawad <sayf4all@hotmail.com>
 */
@Controller
public class ItemController extends ControllerBase implements CrudInterface<Item,Long> {


    @Autowired
  CategoryService categoryService;

  private static final Logger LOG = LoggerFactory.getLogger(ItemController.class);

  // templates
  public static final String TEMPLATE_ITEM_OVERVIEW = "overview_item";
  public static final String TEMPLATE_ITEM_NEW_FORM = "new_item";
  public static final String TEMPLATE_ITEM_SHOW_AND_EDIT_FORM = "new_item";

  @Autowired
  ItemService itemService;


  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  @RequestMapping(value = Paths.ITEM_ADD_NAME_PRICE_DESCRIPTION,
    method = RequestMethod.POST)
  @ResponseBody
  public Map addItem(
          @RequestParam("itemName") String name,
          @RequestParam("priceInput") String price,
          @RequestParam("shortDescription") String description,
          @RequestParam("categoryId") String categoryId
          ) {

    LOG.info("nieuwe item om toe te voegen, Naam:" + name + " /Prijs:" + price + " /beschrijving" + description);
    JSONObject results = new JSONObject();
    if (notEmpty(name) && notEmpty(price) && notEmpty(description)) {
      results.put("success", itemService.createItem(name, price, description, Long.parseLong(categoryId)));
    }
    Map jsonResult = new HashMap();
    jsonResult.put("success", itemService.createItem(name, price, description, Long.parseLong(categoryId)));
    return jsonResult; // results.toJSONString();
  }


  @RequestMapping(value = Paths.ITEM_FORM)
  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  public String create(Model model) {
    String pageTitel = "new item";
    setPageAttributes(model, pageTitel);
    model.addAttribute("action", "new");
    model.addAttribute("categories", categoryService.getCategoryOverviewList());
    return TEMPLATE_ITEM_NEW_FORM;
  }


  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  @RequestMapping(value = Paths.ITEM_DELETE_ID, method = RequestMethod.GET)
  @ResponseBody
  public String deleteItem(@PathVariable(value = "id") Long id) {
    LOG.info("item om te verwijderen met id:" + id);
    JSONObject results = new JSONObject();
    results.put("success", itemService.deleteItem(id));
    return results.toJSONString();
  }



  @RequestMapping(value = Paths.ITEM_EDIT, method = RequestMethod.GET)
  public String editItem(Model model, @PathVariable(value = "id") Long id) {
    String pageTitel = "new item";
    setPageAttributes(model, pageTitel);
    model.addAttribute("action", "edit");

    LOG.info("item om te editen met id:" + id);

    if (id != null) {
      Item item = itemService.getItemById(id);

      if (item != null) {
        model.addAttribute("id", item.getId());
        model.addAttribute("name", item.getName());
        model.addAttribute("price", item.getPrice());
        model.addAttribute("description", item.getShortDescription());
        return TEMPLATE_ITEM_SHOW_AND_EDIT_FORM;
      }
    }
    return TEMPLATE_ITEM_OVERVIEW;
  }


  @RequestMapping(value = Paths.ITEM_OVERVIEW)
  public String getItemOverVieuw(
      @RequestParam(value = "page", required = false, defaultValue = "0") Integer pageNumber,
      @RequestParam(value = "direction", required = false, defaultValue = "asc") String direction,
      @RequestParam(value = "sort", required = false, defaultValue = "username") String sort, Model model) {

    model.addAttribute("direction", direction);
    model.addAttribute("sort", sort);
    String pageTitel = "item overview";
    setPageAttributes(model, pageTitel);
    List<Item> itemList = itemService.getItemOverviewList();
    model.addAttribute("actionType", "itemOverview");
    model.addAttribute("itemList", itemList);

    return TEMPLATE_ITEM_OVERVIEW;
  }


  private boolean notEmpty(String s) {
    if (s != null && s.length() > 0) return true;
    return false;
  }


  @Override
  public Item add(Item object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Item get(Long id) {
    return itemService.getItemById(id);  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public List<Item> getAll() {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Item update(Item object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Boolean remove(Item object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }
}
