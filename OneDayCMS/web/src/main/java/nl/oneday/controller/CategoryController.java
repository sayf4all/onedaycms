package nl.oneday.controller;


import nl.oneday.data.domain.items.Category;
import nl.oneday.data.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 5-7-13 Time: 13:54 To change this template use File | Settings | File Templates.
 */
@Controller
public class CategoryController extends ControllerBase implements CrudInterface<Category,Long> {
  private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
  public static final String CATEGORY_OVERVIEW = "overview_category";
  public static final String CATEGORY_NEW = "new_category";
  public static final String REDIRECT_TO_ROOT = "redirect:/";

  @Autowired
  CategoryService categoryService;


  @RequestMapping(value = "/category/list")
  @ResponseBody
  public List<Category> getCategoryOverView() {
    return categoryService.getCategoryOverviewList();
  }


  @RequestMapping(value = Paths.CATEGORY_OVERVIEW)
  public String getCategoryOverViewList(HttpServletResponse response, Model model) {
    String pageTitle = "overview";
    setPageAttributes(model, pageTitle);
    model.addAttribute("categories", categoryService.getCategoryOverviewList());
    return CATEGORY_OVERVIEW;
  }


  @RequestMapping(value = Paths.CATEGORY_NEW)
  public String newCategory(HttpServletResponse response, Model model) {
    String pageTitle = "new";
    setPageAttributes(model, pageTitle);
    return CATEGORY_NEW;
  }


  @RequestMapping(value = Paths.CATEGORY_ADD)
  public String addCategory(HttpServletRequest request, HttpServletResponse response, Model model) {

    Category category = new Category();
    category.setName(request.getParameter("categoryName"));

    if (request.getParameter("categoryName") != null && !request.getParameter("categoryName").isEmpty()) {
      boolean success = categoryService.createCategory(category);
      if (success) {
        LOG.info("category om toegevoegd met naam:" + request.getParameter("name"));
      }
      model.addAttribute("categoryCreated", success);
    }
    return REDIRECT_TO_ROOT;
  }

    @RequestMapping(value = Paths.CATEGORY_GET_ID)
    public String getCategory(HttpServletRequest request, HttpServletResponse response, Model model) {
        String pageTitle = "overview";
        setPageAttributes(model, pageTitle);
        model.addAttribute("categories", categoryService.getCategoryOverviewList());
        return CATEGORY_OVERVIEW;
    }


  @Override
  public Category add(Category object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Category get(Long id) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public List<Category> getAll() {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Category update(Category object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Boolean remove(Category object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }
}
