package nl.oneday.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 25-2-13 Time: 10:20 To change this template use File | Settings | File
 * Templates.
 */
@Controller
public class HomeController extends ControllerBase {
  private static final Logger LOG = LoggerFactory.getLogger(VersionController.class);

  public static final String TEMPLATE_HOME = "index";

  @Autowired
  private ResourceLoader resourceLoader;


  @RequestMapping(value = "/")
  public String showRevision(Model model) {
    String pageTitle = "home";
    setPageAttributes(model, pageTitle);
    return TEMPLATE_HOME;
  }


}
