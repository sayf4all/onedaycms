package nl.oneday.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 4-6-13 Time: 10:10 To change this template use File | Settings | File Templates.
 */
@Controller
public class ContactController extends ControllerBase {


  public static final String CONTACT_CONTACT_TEMPLATE = "contact";


  @RequestMapping(value = Paths.CONTACT)
  public String showContactPage(Model model) {
    String pageTitle = "contact";
    setPageAttributes(model, pageTitle);
    return CONTACT_CONTACT_TEMPLATE;
  }

}
