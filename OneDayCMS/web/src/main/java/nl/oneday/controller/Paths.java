package nl.oneday.controller;

/**
 * Created with IntelliJ IDEA.
 * User: sayf
 * Date: 7-12-13
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
public class Paths {
  public static final String ROOT = "/";
  public static final String ITEM_FORM = "/item/new";
  public static final String ITEM_OVERVIEW = "/item/overview";
  public static final String ITEM_ADD_NAME_PRICE_DESCRIPTION = "/item/add";
  public static final String ITEM_EDIT = "/item/edit/{id}";
  public static final String ITEM_DELETE_ID = "/item/delete/{id}";
  public static final String CATEGORY_OVERVIEW = "/category/overview";
  public static final String CATEGORY_NEW = "/category/new";
  public static final String CATEGORY_ADD = "/category/add";
  public static final String CATEGORY_GET_ID = "/category/get/{id}";
  public static final String CONTACT = "/contact";
  public static final String ERROR_PAGE_404 = "/404";
  public static final String ERROR_PAGE_500 = "/500";
  public static final String SUCCESS_MESSAGES = "messages";
  public static final String ERROR_MESSAGES = "errors";
  public static final String UPLOAD = "/upload";
  public static final String DATA_GET_BY_ID = "/data/get/{id}";
  public static final String DATA_GET_MINI_BY_ID = "/data/get/mini/{id}";
  public static final String DATA_GET_PROFILE_BY_ID = "/data/get/profile/{id}";
  public static final String HTTP_403 = "/403";
  public static final String HTTP_404 = "/404";
  public static final String HTTP_500 = "/500";
  public static final String USER_ADD = "/user/add";
  public static final String USER_NEW = "/user/new";
  public static final String USER_PROFILE = "/user/profile";
  public static final String USER_DELETE_USERNAME = "/user/delete/{username}";
  public static final String USER_SETPROFILEIMAGE_IMAGE_ID = "/user/setprofileimage/{imageId}";
  public static final String USER_EDIT_USERNAME = "/user/edit/{username}";
  public static final String USER_OVERVIEW = "/user/overview";
  public static final String LOGOUT_SUCCESS = "/logout/success";
  public static final String LOGIN_FAILURE = "/login/failure";
  public static final String ACCESS_DENIED = "/accessDenied";
  public static final String LOGIN = "/login";
}
