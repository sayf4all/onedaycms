package nl.oneday.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 3-6-13 Time: 13:24 To change this template use File | Settings | File Templates.
 */
@Service("accessDeniedHandler")
public class AccessDeniedHandler extends AccessDeniedHandlerImpl {

  private static final Logger LOG = LoggerFactory.getLogger(AccessDeniedHandler.class);


  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception)
      throws IOException, ServletException {
    LOG.info("############### Access Denied Handler!");
    setErrorPage("/accessDenied");
    super.handle(request, response, exception);
  }
}
