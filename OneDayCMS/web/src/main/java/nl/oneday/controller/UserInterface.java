package nl.oneday.controller;


import nl.oneday.data.domain.uploads.File;
import nl.oneday.data.domain.users.ProfileImage;
import nl.oneday.data.domain.users.User;
import nl.oneday.data.service.FileService;
import nl.oneday.data.service.ProfileImageService;
import nl.oneday.data.service.UserService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * Web interface handling for Subscription.
 * 
 * @author Sayf Jawad <sayf4all@hotmail.com>
 */
@Controller
public class UserInterface extends ControllerBase implements CrudInterface<User,Long> {

  private static final Logger LOG = LoggerFactory.getLogger(UserInterface.class);

  // templates
  public static final String TEMPLATE_USER_OVERVIEW = "overview_user";
  public static final String TEMPLATE_USER_NEW_FORM = "new_user";
  public static final String TEMPLATE_USER_PROFILE = "profile";
  public static final String TEMPLATE_USER_SHOW_AND_EDIT_FORM = "new_user";

  @Autowired
  UserService userService;
  @Autowired
  FileService fileService;
  @Autowired
  ProfileImageService profileImageService;


  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  @RequestMapping(value = Paths.USER_ADD, method = RequestMethod.POST)
  public String addUser(Model model, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName,
      @RequestParam("userName") String userName, @RequestParam("password") String password,
      @RequestParam("roleType") String roleType

  ) {

    LOG.info("nieuwe user om toe te voegen, firstname:" + firstName + " lastname:" + lastName + " username:" + userName
        + " roletype:" + roleType);

    if (StringUtils.hasLength(firstName) && StringUtils.hasLength(lastName) && StringUtils.hasLength(userName)
        && StringUtils.hasLength(password) && StringUtils.hasLength(roleType)) {
      String pageTitel = "new user";
      setPageAttributes(model, pageTitel);
      if (userService.createUser(firstName, lastName, userName, password, roleType)) {
        model.addAttribute("userCreated", true);
      }
    }

    return "redirect:/user/new";
  }


  @RequestMapping(value = Paths.USER_NEW)
  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  public String create(Model model, HttpServletRequest request) {
    String pageTitel = "new user";
    setPageAttributes(model, pageTitel);
    model.addAttribute("action", "new");

    if (request.getParameter("userCreated") != null && request.getParameter("userCreated").equals("true")) {
      model.addAttribute("userCreated", "true");
    }
    return TEMPLATE_USER_NEW_FORM;
  }


  @PreAuthorize("(hasAnyRole('ROLE_USER','ROLE_ADMIN'))")
  @RequestMapping(value = Paths.USER_PROFILE)
  public String profile(Model model) {
    String pageTitel = "user profile";
    UserDetails userDetails = getCurrentUserDetails();
    User user = userService.getUserByUserName(userDetails.getUsername());


    model.addAttribute("id", user.getId());
    model.addAttribute("username", user.getUsername());
    model.addAttribute("firstname", user.getFirstName());
    model.addAttribute("lastname", user.getLastName());
    model.addAttribute("fileList", fileService.listAll());

    Long profileImageId = null;

    model.addAttribute("action", "viewProfile");
    setPageAttributes(model, pageTitel);
    return TEMPLATE_USER_PROFILE;
  }


  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  @RequestMapping(value = Paths.USER_DELETE_USERNAME, method = RequestMethod.GET)
  @ResponseBody
  public String deleteUser(@PathVariable(value = "username") String userName) {
    LOG.info("User om te verwijderen met userName:" + userName);
    JSONObject results = new JSONObject();
    results.put("success", userService.deleteUser(userName));
    return results.toJSONString();
  }


  @PreAuthorize("(hasAnyRole('ROLE_USER','ROLE_ADMIN'))")
  @RequestMapping(value = Paths.USER_SETPROFILEIMAGE_IMAGE_ID, method = RequestMethod.GET)
  @ResponseBody
  public String setProfileImage(@PathVariable(value = "imageId") Long imageId) {
    UserDetails userDetails = getCurrentUserDetails();
    User user = userService.getUserByUserName(userDetails.getUsername());
    JSONObject results = new JSONObject();
    File file = fileService.find(imageId);

      ProfileImage profileImage = new ProfileImage();
      profileImage.setFile(file);
      results.put("success", profileImageService.saveProfileImage(profileImage));

    return results.toJSONString();
  }


  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  @RequestMapping(value = Paths.USER_EDIT_USERNAME, method = RequestMethod.GET)
  public String editUser(Model model, @PathVariable(value = "username") String userName) {
    String pageTitel = "edit user";
    setPageAttributes(model, pageTitel);
    LOG.info("User om te editen met userName:" + userName);

    if (userName != null) {
      User user = userService.getUserByUserName(userName);

      if (user != null) {
        model.addAttribute("id", user.getId());
        model.addAttribute("username", user.getUsername());
        model.addAttribute("firstname", user.getFirstName());
        model.addAttribute("lastname", user.getLastName());
        model.addAttribute("password", "********");
        model.addAttribute("action", "edit");
        return TEMPLATE_USER_SHOW_AND_EDIT_FORM;
      }
    }
    return TEMPLATE_USER_OVERVIEW;
  }


  @RequestMapping(value = Paths.USER_OVERVIEW)
  @PreAuthorize("(hasRole('ROLE_ADMIN'))")
  public String getUserOverVieuw(@RequestParam(value = "page", required = false, defaultValue = "0") Integer pageNumber,
      @RequestParam(value = "direction", required = false, defaultValue = "asc") String direction,
      @RequestParam(value = "sort", required = false, defaultValue = "username") String sort, Model model) {
    model.addAttribute("direction", direction);
    model.addAttribute("sort", sort);
    String pageTitel = "user overview";
    setPageAttributes(model, pageTitel);

    List<User> userList = userService.getUserOverviewList();
    model.addAttribute("actionType", "userOverview");
    model.addAttribute("userList", userList);

    return TEMPLATE_USER_OVERVIEW;
  }


  @Override
  public User add(User object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public User get(Long id) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public List<User> getAll() {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public User update(User object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public Boolean remove(User object) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }
}
