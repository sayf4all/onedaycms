package nl.oneday.controller.util;


import java.util.ArrayList;
import java.util.List;

import org.springframework.ui.Model;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * Helper class for controllers.
 * 
 * @author Sayf Jawad <sayf4all@hotmail.com>
 */
public final class ControllerUtil {

  public static final String SUCCESS_MESSAGES = "messages";
  public static final String ERROR_MESSAGES = "errors";


  @SuppressWarnings("unchecked")
  public static void addMessage(RedirectAttributes redirectAttributes, String successMessage) {
    List<String> messages = ((List<String>) redirectAttributes.getFlashAttributes().get(SUCCESS_MESSAGES));
    if (messages == null) {
      messages = new ArrayList<String>();
      redirectAttributes.addFlashAttribute(SUCCESS_MESSAGES, messages);
    }
    messages.add(successMessage);
  }


  @SuppressWarnings("unchecked")
  public static void addError(RedirectAttributes redirectAttributes, String errorMessage) {
    List<String> messages = ((List<String>) redirectAttributes.getFlashAttributes().get(ERROR_MESSAGES));
    if (messages == null) {
      messages = new ArrayList<String>();
      redirectAttributes.addFlashAttribute(ERROR_MESSAGES, messages);
    }
    messages.add(errorMessage);
  }


  /**
   * adds a message to the list of messages in the page model. The list of messages is created when it doesn't exist yet. Use this
   * method when adding a message before a forward.
   * 
   * @param model Contains the error messages.
   * @param errorMessage The error message.
   */
  @SuppressWarnings("unchecked")
  public static void addMessage(Model model, String errorMessage) {
    if (!model.containsAttribute(SUCCESS_MESSAGES)) {
      model.addAttribute(SUCCESS_MESSAGES, new ArrayList<String>());
    }
    List<String> messages = (List<String>) model.asMap().get(SUCCESS_MESSAGES);
    messages.add(errorMessage);
  }


  /**
   * Adds an error to the list of errors on the page model. the list of errors is created when it doesn't exist yet. Use this
   * method when adding an error before a forward.
   * 
   * @param model Contains the error messages.
   * @param errorMessage The error message.
   */
  @SuppressWarnings("unchecked")
  public static void addError(Model model, String errorMessage) {
    if (!model.containsAttribute(ERROR_MESSAGES)) {
      model.addAttribute(ERROR_MESSAGES, new ArrayList<String>());
    }
    List<String> errors = (List<String>) model.asMap().get(ERROR_MESSAGES);
    errors.add(errorMessage);
  }


  /**
   * Factory method for constructing FieldErrors.
   * 
   * @param objectName The subject.
   * @param fieldName The field of the subject.
   * @param errors The occurred errors.
   * @return The error object.
   */
  public static FieldError createFieldError(String objectName, String fieldName, String... errors) {
    return new FieldError(objectName, fieldName, fieldName, true, errors, null, null);
  }
}
