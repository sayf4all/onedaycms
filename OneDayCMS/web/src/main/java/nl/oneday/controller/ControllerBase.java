package nl.oneday.controller;


import nl.oneday.data.domain.items.Category;
import nl.oneday.data.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Basics for all controllers including the following:
 * <ul>
 * <li>The users's full name</li>
 * <li>All the related URLs for the current page</li>
 * <li>Links to the error pages</li>
 * <li>Error messages for forms</li>
 * <li>Success messages for redirects</li>
 * </ul>
 * 
 * @author Sayf Jawad <sayf4all@hotmail.com>
 */
public abstract class ControllerBase {

  private static final Logger LOG = LoggerFactory.getLogger(ControllerBase.class);
  public static final String URLS_NAME = "urls";
  protected Map<String, String> urls = new HashMap<String, String>();
  @Autowired
  CategoryService categoryService;


  // -- Utility methods
  public static String redirect(String url) {
    return String.format("redirect:%s", url);
  }


  public static String forward(String url) {
    return String.format("forward:%s", url);
  }


  public static boolean hasErrors(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return inputFlashMap != null && inputFlashMap.containsKey(Paths.ERROR_MESSAGES);
  }


  @SuppressWarnings("unchecked")
  public static List<String> getErrors(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return (List<String>) inputFlashMap.get(Paths.ERROR_MESSAGES);
  }


  public static boolean hasMessages(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return inputFlashMap != null && inputFlashMap.containsKey(Paths.SUCCESS_MESSAGES);
  }


  @SuppressWarnings("unchecked")
  public static List<String> getMessages(HttpServletRequest request) {
    Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
    return (List<String>) inputFlashMap.get(Paths.SUCCESS_MESSAGES);
  }


  public static boolean hasErrors(HttpSession session) {
    return session.getAttribute(Paths.ERROR_MESSAGES) != null;
  }


  public static Object getErrors(HttpSession session) {
    return session.getAttribute(Paths.ERROR_MESSAGES);
  }


  public static boolean hasMessages(HttpSession session) {
    return session.getAttribute(Paths.SUCCESS_MESSAGES) != null;
  }


  public static Object getMessages(HttpSession session) {
    return session.getAttribute(Paths.SUCCESS_MESSAGES);
  }


  /**
   * Adds an error to the list of errors on the page model. the list of errors is created when it doesn't exist yet. Use this
   * method when adding an error before a forward.
   * 
   * @param model Contains the error messages.
   * @param errorMessage The error message.
   * @deprecated Moved to ControllerUtil.
   */
  @SuppressWarnings("unchecked")
  @Deprecated
  public static void addError(Model model, String errorMessage) {
    if (!model.containsAttribute(Paths.ERROR_MESSAGES)) {
      model.addAttribute(Paths.ERROR_MESSAGES, new ArrayList<String>());
    }
    List<String> errors = (List<String>) model.asMap().get(Paths.ERROR_MESSAGES);
    errors.add(errorMessage);
  }


  /**
   * adds a message to the list of messages in the page model. The list of messages is created when it doesn't exist yet. Use this
   * method when adding a message before a forward.
   * 
   * @param model Contains the error messages.
   * @param errorMessage The error message.
   * @deprecated Moved to ControllerUtil.
   */
  @SuppressWarnings("unchecked")
  @Deprecated
  public static void addMessage(Model model, String errorMessage) {
    if (!model.containsAttribute(Paths.SUCCESS_MESSAGES)) {
      model.addAttribute(Paths.SUCCESS_MESSAGES, new ArrayList<String>());
    }
    List<String> messages = (List<String>) model.asMap().get(Paths.SUCCESS_MESSAGES);
    messages.add(errorMessage);
  }


  /**
   * adds a message to the list of messages in the Flash Attributes. The list of messages is created when it doesn't exist yet.
   * Use this method when adding a message before a redirect.
   * 
   * @param redirectAttributes Contains the success messages.
   * @param successMessage The success message.
   */
  @SuppressWarnings("unchecked")
  public static void addMessage(RedirectAttributes redirectAttributes, String successMessage) {
    List<String> messages = ((List<String>) redirectAttributes.getFlashAttributes().get(Paths.SUCCESS_MESSAGES));
    if (messages == null) {
      messages = new ArrayList<String>();
      redirectAttributes.addFlashAttribute(Paths.SUCCESS_MESSAGES, messages);
    }
    messages.add(successMessage);
  }


  public UserDetails getCurrentUserDetails() {
    SecurityContext securityContext = SecurityContextHolder.getContext();
    Authentication authentication = securityContext.getAuthentication();
    if (authentication != null) {
      Object principal = authentication.getPrincipal();
      return principal instanceof UserDetails ? (UserDetails) principal : null;
    }
    return null;
  }


  public void setPageAttributes(Model model, String pageTitel) {
    UserDetails userDetails = getCurrentUserDetails();
    List<Category> categories = categoryService.getCategoryOverviewList();
    model.addAttribute("categories", categories);
    if (userDetails != null) {
      model.addAttribute("loggedIn", true);
      model.addAttribute("userName", userDetails.getUsername());
      model.addAttribute("pagetitel", pageTitel);
      model.addAttribute("authorisationLevels", userDetails.getAuthorities());
    }
  }


  private void tryToAddUserDetails(ModelAndView mav) {
    UserDetails userDetails = getCurrentUserDetails();
    if (userDetails != null) {
      mav.getModel().put("loggedIn", true);
      mav.getModel().put("userName", userDetails.getUsername());
    }
  }


  @ExceptionHandler(Exception.class)
  public ModelAndView handleExceptions(Exception anExc) {
    ModelAndView mav = new ModelAndView();
    tryToAddUserDetails(mav);

    if (anExc instanceof AccessDeniedException) {
      LOG.warn("access is denied");
      mav.setViewName("403");
    }
    else if (anExc instanceof DataAccessException) {
      LOG.warn("data access failed");
      mav.setViewName("500");
    }
    else if (anExc instanceof NoSuchRequestHandlingMethodException) {
      LOG.warn("NoSuchRequestHandlingMethodException exception");
      mav.setViewName("404");
    }
    else if (anExc instanceof TypeMismatchException) {
      LOG.warn("TypeMismatchException exception");
      mav.setViewName("500");
    }
    else if (anExc instanceof MissingServletRequestParameterException) {
      LOG.warn("MissingServletRequestParameterException exception", anExc);
      mav.setViewName("404");
    }
    else if (anExc instanceof NoSuchRequestHandlingMethodException) {
      LOG.warn("NumberFormatException exception");
      mav.setViewName("500");
    }
    else {
      LOG.error("uncaughtException exception!: " + anExc.getMessage() + "\n\r" + anExc.getStackTrace());
      mav.setViewName("500");
    }
    return mav;
  }
}
