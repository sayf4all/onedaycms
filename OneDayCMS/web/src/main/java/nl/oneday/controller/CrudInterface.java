package nl.oneday.controller;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sayf
 * Date: 11-12-13
 * Time: 23:53
 * To change this template use File | Settings | File Templates.
 */
public interface CrudInterface<T, K> {

  public T add(T object);

  public T get(K id);

  public List<T> getAll();

  public T update(T object);

  public Boolean remove(T object);
}
