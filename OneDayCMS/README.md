# TXOSubscription

## Quick Launch

Launch Tomcat to run the portal locally. By default Tomcat will use the default settings which are stored
under `core/src/main/resources/application(_override).properties`.

The following fixtures will be created:

Users

* admin@sayf.nl @ password    : ROLE_ADMIN
* user@sayf.nl     : ROLE_USER

## Customized Launch Settings

If you wish to provide customized settings, such as a different database or smtp server, please
make a copy of `core/src/main/resources/application_override.properties.template` without
the `.template` suffix and adjust the values to your own needs.

## Localization

By default the messages will be loaded from resources/messages/messages_nl.properties
In the FreeMarker Templates these are accessible by the following macro `<@spring.message 'message.key.name'/>`

## Workflow

This project uses a _pull request workflow_. In short, this means that no direct commits are allowed to the master branch, 
but changes are merged from a so-called feature branch. The motivation behind this is to keep the master branch deployable
at all times. 

# Pull requests

Here is an example scenario of implementing a new feature called `#4: Combine and minify CSS and JavaScript`

1. Create a new issue on Bitbucket if it doesn't exist yet.
2. Create a feature branch with a descriptive name with `git branch minify-css-js`
3. Switch to the new branch with `git checkout minify-css-js`
4. Commit your changes to the new branch with `git commit -a -m "#4 Configured yui-compressor"`
5. Push your changes with `git push origin yui-compressor`
6. Go to Bitbucket and create a pull request by clicking on `-> create pull request`, select `minify-css-js` as source and
    `master` as target. 
    
    Set the title of the pull request to the title of the ticket, which in this case is `#4: Combine and minify CSS and
    JavaScript` 
  
    In the description field, explain the purpose of the pull request and what you expect from the reviewers: "CSS and JS 
    files are now minified using the YUICompressor. Please verify that both `static/css/all.css` and `static/js/all.js` exist".
  
    Click on `Send pull request` to notify the team members.
 
7. The pull request is now ready for review and should be reviewed by **at least 1 team member** other than the initiator. 
    The reviewer can now review the branch by pulling the changes with `git pull` and switching to the feature branch 
    with `git checkout minify-css-cs`
    
    The following is expected from the reviewer : 

        * Code review when applicable
        * Verifying that the added functionality works as explained in the pull request description
        * Verifying that the new feature doesn't break the application
        * Verifying that the new feature doesn't break any tests

    Based on the result of the review, the reviewer can `accept and merge` the pull request or `reject` it. Accepting a pull
    request means sharing responsibility; both initiating and reviewing parties are responsible for the changes.
    
# Commits

Every commit should start with an issue ID followed by a description. The description should be just
enough to cover the activities of a certain commit. Prevent large commits. A simple way of achieving this is to
keep your commit messages at a tweetable size of 140 characters. If you need more than 140 characters to explain
your commit, this might be an indication that the commit is too large.

