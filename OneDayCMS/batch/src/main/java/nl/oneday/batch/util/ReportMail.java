package nl.oneday.batch.util;

import lombok.Getter;
import lombok.Setter;
import nl.oneday.batch.domain.report.Report;
import org.springframework.mail.SimpleMailMessage;

@Setter
@Getter
public class ReportMail {

	private String[] cc;

	private String from;

	private String subject;

	private String[] to;

	public SimpleMailMessage createEmail(Report report) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject(subject);
		message.setTo(to);
		if (cc != null && cc.length > 0) {
			message.setCc(cc);
		}
		message.setFrom(from);

		StringBuilder sb = new StringBuilder();
		sb.append("Totaal aantal abonnementen:");
		sb.append(report.getTotalSubscriptions());
		sb.append("\n");

		sb.append("Totaal aantal gedeactiveerde abonnementen:");
		sb.append(report.getDeletedSubscriptions());
		sb.append("\n");

		sb.append("Totaal aantal nieuwe abonnementen sinds vorige maand (23e)");
		sb.append(report.getNewSubscriptions());
		sb.append("\n");

		sb.append("Totaal aantal CLIEOP incasso regels:");
		sb.append(report.getMonthlySubscriptions());
		sb.append("\n");

		sb.append("Totaal aantal abonnementen naar Zelos:");
		sb.append(report.getMonthlySubscriptions());
		sb.append("\n");

		message.setText(sb.toString());

		return message;
	}

}
