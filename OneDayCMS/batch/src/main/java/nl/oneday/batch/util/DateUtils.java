package nl.oneday.batch.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author jpmjacobs
 * 
 */
public class DateUtils {

	private static Calendar getLastMonth() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		return cal;
	}

	/**
	 * Formats the given <code>Date</code> into a date string and returns this
	 * string.
	 * 
	 * @param date
	 * @return the formatted date string
	 */
	public static String convertDate(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("ddMMyy");
		return simpleDateFormat.format(date);
	}

	/**
	 * Returns last months dutch short name combined with the year e.g. jan
	 * 2012.
	 * 
	 * @return month year string
	 */
	public static String getShortMonthYear() {
		Calendar cal = getLastMonth();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM yyyy", new Locale("nl", "NL"));
		return simpleDateFormat.format(cal.getTime());
	}

	/**
	 * Returns last months dutch full name combined with the year e.g. januari
	 * 2012.
	 * 
	 * @return month year string
	 */
	public static String getFullMonthYear() {
		Calendar cal = getLastMonth();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM yyyy", new Locale("nl", "NL"));
		return simpleDateFormat.format(cal.getTime());
	}

	/**
	 * Returns the current date time in the format 2001-12-31T12:00:00.
	 * 
	 * @return the date string
	 */
	public static String getDateTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return df.format(new Date());
	}
	
	/**
	 * Returns the given date in the format dd-MM-yyyy
	 * @param date
	 * @return the formated date
	 */
	public static String getShortDateFormat(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(date);
	}
	
	/**
	 * Returns the time HH:mm of a given date
	 * @param date
	 * @return the time
	 */
	public static String getShortTimeFormat(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		return df.format(date);
	}
}