package nl.oneday.batch.domain.report;

import java.io.Serializable;

public class Report implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long totalSubscriptions;
	
	private Long deletedSubscriptions;
	
	private Long newSubscriptions;
	
	private Long monthlySubscriptions;

	public Long getTotalSubscriptions() {
		return totalSubscriptions;
	}

	public void setTotalSubscriptions(Long totalSubscriptions) {
		this.totalSubscriptions = totalSubscriptions;
	}

	public Long getDeletedSubscriptions() {
		return deletedSubscriptions;
	}

	public void setDeletedSubscriptions(Long deletedSubscriptions) {
		this.deletedSubscriptions = deletedSubscriptions;
	}

	public Long getNewSubscriptions() {
		return newSubscriptions;
	}

	public void setNewSubscriptions(Long newSubscriptions) {
		this.newSubscriptions = newSubscriptions;
	}

	public Long getMonthlySubscriptions() {
		return monthlySubscriptions;
	}

	public void setMonthlySubscriptions(Long monthlySubscriptions) {
		this.monthlySubscriptions = monthlySubscriptions;
	}
	
	
	
	
}
