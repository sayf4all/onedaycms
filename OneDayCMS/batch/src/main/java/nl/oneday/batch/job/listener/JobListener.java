package nl.oneday.batch.job.listener;

/**
 * Created with IntelliJ IDEA.
 * User: Sayf.Jawad
 * Date: 8/27/13
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */

import nl.oneday.batch.util.ReportMail;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.mail.MailSender;
//import org.springframework.mail.MailSender;

public class JobListener implements JobExecutionListener {

    private ReportMail mail;

    private MailSender mailSender;

   // private SubscriptionDao subscriptionDao;
    public static void main(String[] args){

    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        // Send report email
        //Report report = subscriptionDao.getReport();
        //mailSender.send(mail.createEmail(report));

        // Clean up table
        //subscriptionDao.cleanValidatedSubscriber();
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        // Nothing to do...
    }

    public ReportMail getMail() {
        return mail;
    }


    public void setMail(ReportMail mail) {
        this.mail = mail;
    }

    public MailSender getMailSender() {
        return mailSender;
    }

    public void setMailSender(MailSender mailSender) {
        this.mailSender = mailSender;
    }

}
