package nl.oneday.batch.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author jpmjacobs
 * 
 */
public class StringUtils {
	public enum FillerDirection {
		LEFT_TO_RIGHT, RIGHT_TO_LEFT
	}

	private static String createFormat(boolean numeric, int length, FillerDirection direction) {
		StringBuilder sb = new StringBuilder("%");
		if (numeric) {
			sb.append("0");
		}
		if (direction == FillerDirection.RIGHT_TO_LEFT) {
			sb.append("-");
		}
		sb.append(length);
		sb.append(numeric ? "d" : "s");
		return sb.toString();

	}

	public static String createFixedLengthString(Long input, int length, FillerDirection direction) {
		// Default left to right with zeroes
		if (direction == null) {
			direction = FillerDirection.LEFT_TO_RIGHT;
		}
		return String.format(createFormat(true, length, direction), input);
	}

	public static String createFixedLengthString(String input, int length, FillerDirection direction) {
		if (input == null || input.length() == 0 || input.trim().length() == 0) {
			input = "";
		}
		// Default right to left with spaces
		if (direction == null) {
			direction = FillerDirection.RIGHT_TO_LEFT;
		}

		// Assure input length < length
		if (input.length() > length) {
			input = input.substring(0, length);
		}
		return String.format(createFormat(false, length, direction), input);
	}

	public static String createFiller(int length) {
		return String.format(createFormat(false, length, FillerDirection.LEFT_TO_RIGHT), "");
	}

	public static String convertToValidXml(String input) {
		if (input == null || input.length() == 0) {
			return "";
		}
		return input.replaceAll("&", "&amp;").replace("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;")
				.replaceAll("\'", "&apos;");
	}
	
	public static String convertToCurrencyString(BigDecimal input) {
		if(input == null) {
			input = new BigDecimal("0.00");
		}
		return new DecimalFormat("€ #.##").format(input).toString();
	}
	
	public static String convertEngravedIdToString(Long engravedId) {
		return "";
	}
}
