package nl.oneday.data.domain.items;

import lombok.Getter;
import lombok.Setter;
import nl.oneday.data.domain.uploads.File;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 31-5-13
 * Time: 9:52
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity(name = "item")
public class Item {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private Date date;
  private Float price;
  private String name;
  private String city;
  private String street;
  private String fullDescription;
  private String shortDescription;

  @OneToMany
  private Set<File> fileSet;

  @OneToOne
  private Category category;


}
