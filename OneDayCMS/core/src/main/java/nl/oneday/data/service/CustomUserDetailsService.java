package nl.oneday.data.service;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collection;
import java.util.List;


/**
 * A custom {@link org.springframework.security.core.userdetails.UserDetailsService} where user information is retrieved from a
 * JPA repository
 */
public interface CustomUserDetailsService extends UserDetailsService {


  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;


  public User getUserDetailsObject(nl.oneday.data.domain.users.User user);


  public Collection<? extends GrantedAuthority> grantAuthorities(Long role);


  public List<String> getRoles(Long role);


  public List<GrantedAuthority> getGrantedAuthorities(List<String> roles);

}
