package nl.oneday.data.service.impl;


import nl.oneday.data.domain.items.Category;
import nl.oneday.data.repository.CategoryRepository;
import nl.oneday.data.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 10:02 To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = true)
public class CategoryServiceImpl implements CategoryService {
  private static final Logger LOG = LoggerFactory.getLogger(CategoryServiceImpl.class);

  @Autowired
  private CategoryRepository categoryRepository;


  public boolean categoryExists(Long id) {
    Category category = categoryRepository.findById(id);
    if (category != null) return true;
    return false;
  }


  public boolean deleteCategory(Long id) {
    Category category = null;
    if (categoryExists(id)) {
      try {
        category = categoryRepository.findById(id);
        categoryRepository.delete(category.getId());
        categoryRepository.flush();

        if (categoryRepository.findById(id) == null) {

          LOG.info("Category verwijderd met id: " + id);
          return true;
        }
      }
      catch (Exception ex) {
        LOG.info("Onbekende fout is opgetreden\n\r" + ex.getMessage());
      }

    };
    return false;
  }


  public boolean createCategory(Category category) {

    Category savedCategory = null;
    try {
      savedCategory = categoryRepository.saveAndFlush(category);
    }
    catch (Exception ex) {
      LOG.error("Category could not be saved due to an error");
    }

    if (savedCategory != null && savedCategory.getId() != null) {
      LOG.info("nieuwe Category aangemaakt " + savedCategory.getId() + "/" + category.getName() + "/" + category.getName());
      return true;
    }
    else {
      LOG.error("category niet aangemaakt!");
      return false;
    }
  }

  public Category findCategoryById(Long categoryId){
      return categoryRepository.findById(categoryId);
  }

  public List<Category> getCategoryOverviewList() {
    List<Category> categoryList = null;
    try {
      return (categoryRepository.findAll());
    }
    catch (Exception ex) {
      LOG.error("Category could not find categories");
    }
    return categoryList;
  }
}
