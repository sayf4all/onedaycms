package nl.oneday.data.repository;

import nl.oneday.data.domain.items.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 3-6-13
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
public interface ItemRepository extends JpaRepository<Item, Long> {

    //Page<Item> findList(Pageable pageable);
    List<Item> findAll();
    Item findById(Long id);
    Item save(Item item);
    List<Item> findByName(String name, int page, int pageSize);

}
