package nl.oneday.data.service;


import nl.oneday.data.domain.users.User;

import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 11:02 To change this template use File | Settings | File Templates.
 */
public interface UserService {


  public boolean createUser(String firstName, String lastName, String userName, String password, String roleType);


  public boolean deleteUser(String userName);


  public User getUserByUserName(String userName);


  public List<User> getUserOverviewList();
}
