package nl.oneday.data.domain.items;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: sayf
 * Date: 8-12-13
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity
public class SubCategory {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;

  @OneToOne
  private Category parentCategory;

}
