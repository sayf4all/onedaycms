package nl.oneday.data.service.impl;


import nl.oneday.data.domain.items.Category;
import nl.oneday.data.domain.items.Item;
import nl.oneday.data.repository.ItemRepository;
import nl.oneday.data.service.CategoryService;
import nl.oneday.data.service.ItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 10:02 To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = true)
public class ItemServiceImpl implements ItemService {
  private static final Logger LOG = LoggerFactory.getLogger(ItemServiceImpl.class);

  @Autowired
  private ItemRepository itemRepository;

  @Autowired
  private CategoryService categoryService;


  public boolean itemExists(Long id) {
    Item item = itemRepository.findById(id);
    if (item != null) return true;
    return false;
  }


  public boolean deleteItem(Long id) {
    Item item = null;
    if (itemExists(id)) {
      try {
        item = itemRepository.findById(id);
        itemRepository.delete(item.getId());
        itemRepository.flush();

        if (itemRepository.findById(id) == null) {

          LOG.info("Item verwijderd met id: " + id);
          return true;
        }
      }
      catch (Exception ex) {
        LOG.info("Onbekende fout is opgetreden\n\r" + ex.getMessage());
      }

    }

    return false;
  }


  public boolean createItem(String name, String price, String description) {
    Item item = new Item();
    item.setName(name);
    try {
      item.setPrice(Float.parseFloat(price));
    }
    catch (NumberFormatException ex) {
      LOG.error("parse error occured while parsing price!");
      item.setPrice(0.0f);
    }
    item.setShortDescription(description);
    Item savedItem = null;
    try {
      savedItem = itemRepository.saveAndFlush(item);
    }
    catch (Exception ex) {
      LOG.error("Item could not be saved due to an error");
    }

    if (savedItem != null && savedItem.getId() != null) {
      LOG.info("nieuwe Item aangemaakt " + savedItem.getId() + "/" + savedItem.getName() + "/" + savedItem.getPrice() + "/"
        + savedItem.getShortDescription());
      return true;
    } else {
      LOG.error("Item niet aangemaakt!");
      return false;
    }
  }


  public Item createItem(String name, String price, String description, Long categoryId) {
    Category category = categoryService.findCategoryById(categoryId);
    Item item = new Item();
    item.setName(name);
    item.setCategory(category);
    try {
      item.setPrice(Float.parseFloat(price));
    }
    catch (NumberFormatException ex) {
      LOG.error("parse error occured while parsing price!");
      item.setPrice(0.0f);
    }
    item.setShortDescription(description);
    Item savedItem = null;
    try {
      savedItem = itemRepository.saveAndFlush(item);
    }
    catch (Exception ex) {
      LOG.error("Item could not be saved due to an error");
    }

    if (savedItem != null && savedItem.getId() != null) {
      LOG.info("nieuwe Item aangemaakt " + savedItem.getId() + "/" + savedItem.getName() + "/" + savedItem.getPrice() + "/"
        + savedItem.getShortDescription());
      return savedItem;
    } else {
      LOG.error("Item niet aangemaakt!");
      return null;
    }
  }


  public Item getItemById(Long id) {
    Item item = null;
    try {
      item = itemRepository.findById(id);
    }
    catch (Exception ex) {
      LOG.warn("Eroor bij het ophalen van ");
    }
    if (item != null) {
      LOG.info("Item gevonden id:" + item.getId() + " naam:" + item.getName() + " price:" + item.getPrice() + " beschrijving:"
        + item.getShortDescription());
      return item;
    } else {
      LOG.error("Item niet gevonden!");
      return null;
    }
  }


  public List<Item> getItemOverviewList() {
    List<Item> itemList = null;
    try {
      return (itemRepository.findAll());
    }
    catch (Exception ex) {
      LOG.error("Item could not find items");
    }
    return itemList;
  }
}
