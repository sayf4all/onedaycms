package nl.oneday.data.service;


import nl.oneday.data.domain.items.Category;

import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 10:02 To change this template use File | Settings | File Templates.
 */
public interface CategoryService {


  public boolean categoryExists(Long id);


  public boolean deleteCategory(Long id);


  public boolean createCategory(Category category);

  public Category findCategoryById(Long categoryId);

  public List<Category> getCategoryOverviewList();
}
