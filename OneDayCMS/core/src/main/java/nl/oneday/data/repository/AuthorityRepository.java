package nl.oneday.data.repository;

import nl.oneday.data.domain.users.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
  Authority findById(Long id);

  List<Authority> findAll();

  Authority findByAuthority(Authority authority);
}
