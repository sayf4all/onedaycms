package nl.oneday.data.service.impl;


import nl.oneday.data.domain.users.ProfileImage;
import nl.oneday.data.repository.ProfileImageRepository;
import nl.oneday.data.service.ProfileImageService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 4-7-13 Time: 14:59 To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = true)
public class ProfileImageServiceImpl implements ProfileImageService {
  private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ProfileImageServiceImpl.class);

  @Autowired
  ProfileImageRepository profileImageRepository;


  /**
   * find
   */
  public boolean saveProfileImage(ProfileImage profileImage) {

    try {
      profileImageRepository.saveAndFlush(profileImage);
      return true;
    }
    catch (Exception e) {
      LOG.warn("profile image was not set due to an error!" + e.getStackTrace());
      return false;
    }
  }
}
