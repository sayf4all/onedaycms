package nl.oneday.data.domain.items;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 4-7-13 Time: 10:23 To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity
public class Category {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;
}
