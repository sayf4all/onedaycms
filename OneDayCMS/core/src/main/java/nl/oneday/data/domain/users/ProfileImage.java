package nl.oneday.data.domain.users;

import lombok.Getter;
import lombok.Setter;
import nl.oneday.data.domain.uploads.File;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 14-6-13
 * Time: 12:43
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity
public class ProfileImage {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToOne
    private File file;

}
