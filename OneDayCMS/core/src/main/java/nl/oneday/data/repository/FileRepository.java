package nl.oneday.data.repository;

import nl.oneday.data.domain.uploads.File;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 10-6-13
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */

public interface FileRepository extends JpaRepository<File, Long> {
    //Page<Item> findList(Pageable pageable);
    List<File> findAll();
    File findById(Long id);
}
