package nl.oneday.data.service;


import nl.oneday.data.domain.items.Item;

import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 10:02 To change this template use File | Settings | File Templates.
 */
public interface ItemService {

  public boolean itemExists(Long id);


  public boolean deleteItem(Long id);


  public boolean createItem(String name, String price, String description);


  public Item createItem(String name, String price, String description, Long categoryId);

  public Item getItemById(Long id);


  public List<Item> getItemOverviewList();
}
