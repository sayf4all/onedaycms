package nl.oneday.data.domain.uploads;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 10-6-13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity
public class File {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String filename;
    private String notes;
    private String type;
    private String filePath;

    @GeneratedValue(strategy= GenerationType.AUTO)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

}