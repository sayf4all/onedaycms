package nl.oneday.data.service;


import nl.oneday.data.domain.users.ProfileImage;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 4-7-13 Time: 14:59 To change this template use File | Settings | File Templates.
 */
public interface ProfileImageService {

  public boolean saveProfileImage(ProfileImage profileImage);
}
