package nl.oneday.data.domain.uploads;

import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Setter
@Getter
@JsonIgnoreProperties({"bytes"})
public class FileMeta {

	private String fileName;

  private String fileSize;

  private String fileType;
	
	private byte[] bytes;

}
