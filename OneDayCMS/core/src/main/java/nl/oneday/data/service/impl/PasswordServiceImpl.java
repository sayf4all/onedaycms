package nl.oneday.data.service.impl;


import nl.oneday.data.service.CustomUserDetailsService;
import nl.oneday.data.service.PasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 31-5-13 Time: 11:16 To change this template use File | Settings | File
 * Templates.
 */
@Service
@Transactional(readOnly = true)
public class PasswordServiceImpl implements PasswordService {
  @Autowired
  CustomUserDetailsService customUserDetailsService;


  public String getMD5EncodedPasswordHash(String pass) {
    try {
      PasswordEncoder encoder = new Md5PasswordEncoder();
      String hashedPass = encoder.encodePassword(pass, null);
      return hashedPass.toLowerCase();
    }
    catch (Exception e) {

    }
    return null;
  }


  @Override
  public String encodePassword(String rawPass, Object salt) {
    try {
      PasswordEncoder encoder = new Md5PasswordEncoder();
      String hashedPass = encoder.encodePassword(rawPass, null);
      return hashedPass.toLowerCase();
    }
    catch (Exception e) {

    }
    return null;
  }


  @Override
  public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
    try {
      String hashedPass = this.encodePassword(rawPass, null);
      return hashedPass.toLowerCase().equals(encPass);
    }
    catch (Exception e) {

    }
    return false;
  }


  @Override
  public String encode(CharSequence charSequence) {
    return null;  //To change body of implemented methods use File | Settings | File Templates.
  }


  @Override
  public boolean matches(CharSequence charSequence, String s) {
    return false;  //To change body of implemented methods use File | Settings | File Templates.
  }
}
