package nl.oneday.data.service;


import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 31-5-13 Time: 11:16 To change this template use File | Settings | File
 * Templates.
 */
public interface PasswordService extends PasswordEncoder {


  public String getMD5EncodedPasswordHash(String pass);


  public String encodePassword(String rawPass, Object salt);


  public boolean isPasswordValid(String encPass, String rawPass, Object salt);
}
