package nl.oneday.data.service.impl;


import nl.oneday.data.domain.users.Authority;
import nl.oneday.data.domain.users.User;
import nl.oneday.data.repository.UserRepository;
import nl.oneday.data.service.FileService;
import nl.oneday.data.service.PasswordService;
import nl.oneday.data.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA. User: sjawad Date: 6-6-13 Time: 11:02 To change this template use File | Settings | File Templates.
 */
@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
  private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
  @Autowired
  UserRepository userRepository;

  @Autowired
  FileService fileService;

  @Autowired
  PasswordService passwordServiceImpl;


  public boolean createUser(String firstName, String lastName, String userName, String password, String roleType) {

    User user;

    user = userRepository.findByUsername(userName.toLowerCase());
    if (user == null) {
      user = new User();
      user.setUsername(userName);
      user.setFirstName(firstName);
      user.setLastName(lastName);
      user.setPassword(passwordServiceImpl.getMD5EncodedPasswordHash(password));
      Authority authority = new Authority();

      List<Authority> authorities = new ArrayList<Authority>();
      user.setAuthorities(authorities);
      try {
        user = userRepository.saveAndFlush(user);
      }
      catch (Exception ex) {
        LOG.error("something went wrong while saving user: " + ex.getMessage());
      }
      if (user != null) return true;

    }

    return false; // To change body of created methods use File | Settings | File Templates.
  }


  public boolean deleteUser(String userName) {
    User user = userRepository.findByUsername(userName);
    if (user != null) userRepository.delete(user);
    userRepository.flush();
    user = userRepository.findByUsername(userName);
    if (user == null) return true;
    return false; // To change body of created methods use File | Settings | File Templates.
  }


  public User getUserByUserName(String userName) {

    return userRepository.findByUsername(userName);
  }


  public List<User> getUserOverviewList() {

    return userRepository.findAll();
  }
}
