package nl.oneday.data.repository;

import nl.oneday.data.domain.items.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 3-6-13
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {

    //Page<Item> findList(Pageable pageable);
    List<Category> findAll();
    Category findById(Long id);
    Category save(Category category);
    List<Category> findByName(String name, int page, int pageSize);

}
