package nl.oneday.data.service;


import nl.oneday.data.domain.uploads.File;

import java.io.IOException;
import java.util.List;


public interface FileService {


  public File find(Long id);


  public List<File> listAll();


  public boolean save(final File file);


  public void delete(Long id);


  public byte[] readFile(String file) throws IOException;

  public byte[] readFile(java.io.File file) throws IOException;

}
