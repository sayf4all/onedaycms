package nl.oneday.data.repository;

import nl.oneday.data.domain.users.ProfileImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 10-6-13
 * Time: 16:49
 * To change this template use File | Settings | File Templates.
 */

public interface ProfileImageRepository extends JpaRepository<ProfileImage, Long> {

    List<ProfileImage> findAll();
    ProfileImage findById(Long id);
}
