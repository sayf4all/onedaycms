package nl.oneday.data.repository;

import nl.oneday.data.domain.users.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByUsername(String username);
    User findById(Long id);
    List<User> findAll();

}
