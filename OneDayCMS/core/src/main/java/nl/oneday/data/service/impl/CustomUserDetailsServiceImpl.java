package nl.oneday.data.service.impl;


import nl.oneday.data.repository.UserRepository;
import nl.oneday.data.service.CustomUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * A custom {@link org.springframework.security.core.userdetails.UserDetailsService} where user information is retrieved from a
 * JPA repository
 */
@Service
@Transactional(readOnly = true)
public class CustomUserDetailsServiceImpl implements UserDetailsService, CustomUserDetailsService {
  private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsServiceImpl.class);

  @Autowired
  private UserRepository userRepository;




  /**
   * Returns a populated {@link org.springframework.security.core.userdetails.UserDetails} object. The username is first retrieved
   * from the database and then mapped to a {@link org.springframework.security.core.userdetails.UserDetails} object.
   */
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    try {
      nl.oneday.data.domain.users.User user = userRepository.findByUsername(username);

      return getUserDetailsObject(user);
    }
    catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  public User getUserDetailsObject(nl.oneday.data.domain.users.User user) {
    User springSecurityUserDetails;
    boolean enabled = true;
    boolean accountNonExpired = true;
    boolean credentialsNonExpired = true;
    boolean accountNonLocked = true;
    String name = user.getUsername();
    String password = user.getPassword().toLowerCase();
    List<? extends GrantedAuthority > authorities = user.getAuthorities();

    springSecurityUserDetails = new User(name, password, enabled, accountNonExpired, credentialsNonExpired,
      accountNonLocked, authorities);
    return springSecurityUserDetails;
  }


  /**
   * Retrieves a collection of {@link org.springframework.security.core.GrantedAuthority} based on a numerical role
   * 
   * @param role the numerical role
   * @return a collection of {@link org.springframework.security.core.GrantedAuthority

   */
  public Collection<? extends GrantedAuthority> grantAuthorities(Long role) {
    List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
    return authList;
  }


  /**
   * Converts a numerical role to an equivalent list of roles
   * 
   * @param role the numerical role
   * @return list of roles as as a list of {@link String}
   */
  public List<String> getRoles(Long role) {
    List<String> roles = new ArrayList<String>();

    if (role.intValue() == 1) {
      roles.add("ROLE_USER");
      roles.add("ROLE_ADMIN");

    }
    else if (role.intValue() == 2) {
      roles.add("ROLE_USER");
    }

    return roles;
  }


  /**
   * Wraps {@link String} roles to {@link org.springframework.security.core.authority.SimpleGrantedAuthority} objects
   * 
   * @param roles {@link String} of roles
   * @return list of granted authorities
   */
  public List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
    for (String role : roles) {
      authorities.add(new SimpleGrantedAuthority(role));
    }
    return authorities;
  }

}
