package nl.oneday.data.domain.users;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: sjawad
 * Date: 15-2-13
 * Time: 13:35
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@Entity(name="users")
public class User {
    transient private PasswordEncoder encoder ;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String firstName;
    private String lastName;

    @Column(unique=true)
    private String username;
    private String password;

    @OneToMany
    private java.util.List<Authority> authorities;

}
