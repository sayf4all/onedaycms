package nl.oneday.data.service.impl;

import nl.oneday.data.domain.uploads.File;
import nl.oneday.data.repository.FileRepository;
import nl.oneday.data.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sayf
 * Date: 19-12-13
 * Time: 23:29
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional(readOnly = true)
public class FileServiceImpl implements FileService {
  private static final Logger LOG = LoggerFactory.getLogger(FileServiceImpl.class);

  @Autowired
  FileRepository fileRepository;

  @Value("${upload.directory}")
  String uploadDirectory;


  /**
   * find
   */
  public File find(Long id) {

    try {
      File file = fileRepository.findById(id);

      return file;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    return null;
  }


  /**
   * listAll
   */
  public List<File> listAll() {
    try {
      List<File> files = fileRepository.findAll();
      return files;
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }


  /**
   * save
   */
  public boolean save(final File file) {
    try {
      File savedFile = fileRepository.save(file);
      if (savedFile != null) {
        return true;
      }
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }


  /**
   * delete
   */
  public void delete(Long id) {
    try {
      fileRepository.delete(id);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }


  public byte[] readFile(String file) throws IOException {
    return readFile(new java.io.File(file));
  }


  public byte[] readFile(java.io.File file) throws IOException {
    // Open file
    RandomAccessFile f = new RandomAccessFile(file, "r");
    try {
      // Get and check length
      long longlength = f.length();
      int length = (int) longlength;
      if (length != longlength) throw new IOException("File size >= 2 GB");
      // Read file and return data
      byte[] data = new byte[length];
      f.readFully(data);
      return data;
    }
    finally {
      f.close();
    }
  }

}
