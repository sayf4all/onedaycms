-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 23 aug 2013 om 14:37
-- Serverversie: 5.6.12-log
-- PHP-versie: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `examscam`
--
CREATE DATABASE IF NOT EXISTS `examscam` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examscam`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `timeStamp` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK21699C16009EC` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Gegevens worden uitgevoerd voor tabel `file`
--

INSERT INTO `file` (`id`, `filePath`, `filename`, `notes`, `timeStamp`, `type`, `user_id`) VALUES
(1, 'c:\\Development\\Data\\onedaysayf\\3a3ee37.jpg', '3a3ee37.jpg', NULL, '2013-08-23 16:35:44', 'image/jpeg', 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `itemcategory`
--

CREATE TABLE IF NOT EXISTS `itemcategory` (
  `id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK32432D51884264EC` (`item_id`),
  KEY `FK32432D5166FF400C` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `profileimage`
--

CREATE TABLE IF NOT EXISTS `profileimage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK51852552D727E04C` (`file_id`),
  KEY `FK5185255216009EC` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Gegevens worden uitgevoerd voor tabel `profileimage`
--

INSERT INTO `profileimage` (`id`, `file_id`, `user_id`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK35807616009EC` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden uitgevoerd voor tabel `role`
--

INSERT INTO `role` (`id`, `role`, `user_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3),
(4, 1, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `role_types`
--

CREATE TABLE IF NOT EXISTS `role_types` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `role_types`
--

INSERT INTO `role_types` (`id`, `type`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER'),
(3, 'ROLE_GUEST');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Gegevens worden uitgevoerd voor tabel `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `password`, `username`) VALUES
(1, 'John', 'Smith', '21232f297a57a5a743894a0e4a801fc3', 'john'),
(2, 'Jane', 'Adams', 'ee11cbb19052e40b07aac0ca060c23ee', 'jane'),
(3, 'Sayf', 'Jawad', '1010bb6f32f2e201313cb63f6d651d3e', 'sayf'),
(4, 'Ali', 'Schummager', '1010bb6f32f2e201313cb63f6d651d3e', 'ali');

--
-- Beperkingen voor gedumpte tabellen
--

--
-- Beperkingen voor tabel `file`
--
ALTER TABLE `file`
ADD CONSTRAINT `FK21699C16009EC` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Beperkingen voor tabel `itemcategory`
--
ALTER TABLE `itemcategory`
ADD CONSTRAINT `FK32432D5166FF400C` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
ADD CONSTRAINT `FK32432D51884264EC` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`);

--
-- Beperkingen voor tabel `profileimage`
--
ALTER TABLE `profileimage`
ADD CONSTRAINT `FK5185255216009EC` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
ADD CONSTRAINT `FK51852552D727E04C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`);

--
-- Beperkingen voor tabel `role`
--
ALTER TABLE `role`
ADD CONSTRAINT `FK35807616009EC` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
